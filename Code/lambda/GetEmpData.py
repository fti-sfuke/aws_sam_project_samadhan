import json
import boto3
from boto3.dynamodb.conditions import Key, And, Attr
import logging
import os

ddb = boto3.resource('dynamodb')
table = ddb.Table(os.environ['ENV_EMP_TABLE_NAME'])
logging.getLogger().setLevel(logging.INFO)
logging.info('Loading get employee data lambda function')

def get(event, context):
    logging.info("request : {}".format(str(event)))
    try:
        data = table.scan()['Items']
        return json_response(data)
    except Exception as e:
        logging.error(e)
        return json_response('Fail')

def json_response(message, status_code=200):
    return {
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }
