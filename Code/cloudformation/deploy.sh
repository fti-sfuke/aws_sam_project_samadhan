#!/bin/bash
REALM=${REALM}
CODE_UPLOAD_BUCKET=sam-cloudformation-deploy-${REALM}
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DEFAULT_PYTHON_RUNTIME=python3.8
REGION=${AWS_DEFAULT_REGION}

echo 'Deploying instance'
echo ${INSTANCE}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo ${INSTANCE}
INSTANCE_PATH=$DIR/instances/${INSTANCE}.sh

if [ -f "$INSTANCE_PATH" ]; then
    echo "$INSTANCE_PATH exists."
    . $INSTANCE_PATH
else
    echo "$INSTANCE_PATH does not exist."
    exit 1
fi


function deploy() {
    # change to ui root directory
    UUID=`uuidgen`
    NOW=`date "+%Y%m%d_%H%M%S"`
    CODE_S3_PREFIX="${INSTANCE}/${NOW}/${UUID}"

    pushd ../
    rm ./cloudformation/${CODE_ZIP}

    pushd ./lambda
    pip3 install -r ./requirements.txt --target ./
    zip -q ../cloudformation/${CODE_ZIP} \
    -r \
    . \

    popd
    popd

    if aws s3 ls "s3://${CODE_UPLOAD_BUCKET}" 2>&1 | grep -q 'NoSuchBucket'
    then
        aws s3api create-bucket --bucket ${CODE_UPLOAD_BUCKET} --create-bucket-configuration LocationConstraint=${REGION}
    fi
    # Upload to s3 (code)
    aws --region ${REGION} s3 cp ${CODE_ZIP} s3://${CODE_UPLOAD_BUCKET}/${CODE_S3_PREFIX}/${CODE_ZIP}

    STACK_NAME="sam-cloudformation"-${INSTANCE}

    aws --region ${REGION} cloudformation deploy \
    --template-file template.yml \
    --s3-bucket ${CODE_UPLOAD_BUCKET} \
    --s3-prefix ${CODE_S3_PREFIX} \
    --stack-name ${STACK_NAME} \
    --capabilities CAPABILITY_NAMED_IAM \
    --no-fail-on-empty-changeset \
    --parameter-overrides \
    LambdaCodeS3Bucket=${CODE_UPLOAD_BUCKET} \
    LambdaCodeS3Key="${CODE_S3_PREFIX}/${CODE_ZIP}" \
    RealmName=${REALM} \
    PythonRuntime=${DEFAULT_PYTHON_RUNTIME}

}
deploy
